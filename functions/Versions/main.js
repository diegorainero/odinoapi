const Utilities = require('./utilities');

const httpResult = (code, body) => {
	let bodyStr = JSON.stringify(body);
	return {
		statusCode: code,
		headers: { 'Content-Type': 'application/json' },
		body: bodyStr,
	};
};

exports.handler = async (event) => {
	console.log(JSON.stringify(event));
	let headers = event.headers;
	if (!headers || !headers.Authorization || !Utilities.checkToken(headers.Authorization)) {
		return httpResult(500, { message: 'InternalServerError' });
	}
	try {
		let ret = [
			{
				version: '2.1.1',
				url: 'https://' + process.env.API_URL_ADDR + '/latest/ocpi/cpo/2.1.1',
			},
			// {
			// 	version: '2.2',
			// 	url: 'https://' + process.env.API_URL_ADDR + '/latest/ocpi/2.2',
			// },
		];

		return httpResult(200, ret);
	} catch (err) {
		console.log(err);
		return httpResult(500, { message: 'InternalServerError' });
	}
};
