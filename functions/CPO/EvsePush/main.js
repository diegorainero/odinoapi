const DynamoDBChargeboxes = require('./DynamoDB/chargeboxes');
const DynamoDBOcpiToken = require('./DynamoDB/ocpi-token');
const S3 = require('./S3/certificate');
var fs = require('fs');
const axios = require('axios');
const https = require('https');
//const certif = require('./certs/Enega_signed.pem');
const DynamoDBClients = require('./DynamoDB/clients');

exports.handler = async (event) => {
	//console.log(JSON.stringify(event));

	const bucket = 'dev.documents.thor.tools';
	const key = 'certs/hubject.key';
	const p12 = 'certs/Cert.p12';
	const pem = 'certs/Enega_signed.pem';
	let p12_cert = await S3.getFile(bucket, p12);
	//var certs = fs.readFileSync(p12_cert.Body);
	//console.log(p12_cert.toString('utf-8'));
	const options = {
		method: 'post',
		httpsAgent: new https.Agent({
			passphrase: 'enega',
			//pfx: p12_cert.Body,
			ca: fs.readFileSync(p12_cert),
			rejectUnauthorized: false,
		}),
		// proxy: {
		// 	host: '54.195.41.33',
		// 	port: 8888,
		// },
	};
	// send the request
	let response = await axios.post('https://service-qa.hubject.com/api/oicp/evsepush/v23/operators/ITENM/data-records', options);
	// const response = await axios.request({
	// 	url: '54.195.41.33',
	// 	method: 'post',
	// 	headers: {
	// 		'content-type': 'application/json',
	// 	},
	// 	data: JSON.stringify({}),
	// 	httpsAgent: new https.Agent({
	// 		passphrase: 'enega',
	// 		pfx: CREDENTIALS.certificate,
	// 	}),
	// });
	console.log('response', response);
	return response;
};
