const AWS = require('aws-sdk');
const S3 = new AWS.S3({
	maxRetries: parseInt(process.env.S3_RETRIES),
	retryDelayOptions: parseInt(process.env.S3_RETRY_DELAY_MILLISECONDS),
});

function getFile(bucket, key) {
	const params = {
		Bucket: bucket,
		Key: key,
	};

	return S3.getObject(params).promise();
}

module.exports.getFile = getFile;
