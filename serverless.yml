# Welcome to Serverless!
#
# This file is the main config file for your service.
# It's very minimal at this point and uses default values.
# You can always add more config options for more control.
# We've included some commented out config examples here.
# Just uncomment any of them to get that config option.
#
# For full config options, check the docs:
#    docs.serverless.com
#
# Happy Coding!

service: odino-api

provider:
  name: aws
  profile: smartbit-thor
  versionFunctions: false

  # Defaults
  stage: ${opt:stage, "dev"}
  region: ${opt:region, "eu-west-1"}

  apiGateway:
    description: Some Description

  # usagePlan:
  #   - free:
  #     name: free

plugins:
  - serverless-domain-manager

package:
  excludeDevDependencies: false
  individually: true

custom:
  environment:
    dev: Dev
    prd: Prd

    IAMPolicyVersion: "2012-10-17"

    # Policy document assigned to each Lambda IAM role
    BaseLambdaAssumeRolePolicyDocument:
      Version: ${self:custom.environment.IAMPolicyVersion}
      Statement:
        - Effect: Allow
          Principal:
            Service:
              - lambda.amazonaws.com
          Action: sts:AssumeRole

    CloudWatchLogs:
      BaseArn:
        !Join [
          ":",
          ["arn", "aws", "logs", !Ref AWS::Region, !Ref AWS::AccountId],
        ]
      LogGroups:
        EvsePush:
          Name:
            !Join [
              "",
              [
                "/aws/lambda/",
                "${self:custom.environment.${self:provider.stage}}",
                "OdinoLambdaEvsePush",
              ],
            ]
          Arn:
            !Join [
              ":",
              [
                "${self:custom.environment.CloudWatchLogs.BaseArn}",
                "log-group",
                "${self:custom.environment.CloudWatchLogs.LogGroups.EvsePush.Name}",
                "*",
              ],
            ]
    S3:
      BaseArn: "arn:aws:s3:::"
      Prefix:
        Dev: dev.
        Prd: ""
      Buckets:
        Documents:
          !Join [
            "",
            [
              "${self:custom.environment.S3.Prefix.${self:custom.environment.${self:provider.stage}}}",
              documents.thor.tools,
            ],
          ]
      Arns:
        Assets:
          !Join [
            "",
            ["${self:custom.environment.S3.BaseArn}", "assets.thor.tools"],
          ]
        Documents:
          !Join [
            "",
            [
              "${self:custom.environment.S3.BaseArn}",
              "${self:custom.environment.S3.Buckets.Documents}",
            ],
          ]
    DynamoDB:
      BaseArn:
        !Join [
          ":",
          [
            "arn",
            "aws",
            "dynamodb",
            !Ref AWS::Region,
            !Ref AWS::AccountId,
            table,
          ],
        ]
      TablePrefix: !Join ["", ["${self:provider.stage}", "_thor"]]
      Tables:
        Administrators:
          !Join [
            "",
            [
              "${self:custom.environment.DynamoDB.TablePrefix}",
              "_administrators",
            ],
          ]
        Chargeboxes:
          !Join [
            "",
            ["${self:custom.environment.DynamoDB.TablePrefix}", "_chargeboxes"],
          ]
        Clients:
          !Join [
            "",
            ["${self:custom.environment.DynamoDB.TablePrefix}", "_clients"],
          ]
        TransactionsRunning:
          !Join [
            "",
            [
              "${self:custom.environment.DynamoDB.TablePrefix}",
              "_transactions_running",
            ],
          ]
        TransactionsFinished:
          !Join [
            "",
            [
              "${self:custom.environment.DynamoDB.TablePrefix}",
              "_transactions_finished",
            ],
          ]
        Reservations:
          !Join [
            "",
            [
              "${self:custom.environment.DynamoDB.TablePrefix}",
              "_reservations",
            ],
          ]
      Arns:
        Administrators:
          !Join [
            "/",
            [
              "${self:custom.environment.DynamoDB.BaseArn}",
              "${self:custom.environment.DynamoDB.Tables.Administrators}",
            ],
          ]
        Chargeboxes:
          !Join [
            "/",
            [
              "${self:custom.environment.DynamoDB.BaseArn}",
              "${self:custom.environment.DynamoDB.Tables.Chargeboxes}",
            ],
          ]
        Clients:
          !Join [
            "/",
            [
              "${self:custom.environment.DynamoDB.BaseArn}",
              "${self:custom.environment.DynamoDB.Tables.Clients}",
            ],
          ]
        TransactionsRunning:
          !Join [
            "/",
            [
              "${self:custom.environment.DynamoDB.BaseArn}",
              "${self:custom.environment.DynamoDB.Tables.TransactionsRunning}",
            ],
          ]
        TransactionsFinished:
          !Join [
            "/",
            [
              "${self:custom.environment.DynamoDB.BaseArn}",
              "${self:custom.environment.DynamoDB.Tables.TransactionsFinished}",
            ],
          ]
        Reservations:
          !Join [
            "/",
            [
              "${self:custom.environment.DynamoDB.BaseArn}",
              "${self:custom.environment.DynamoDB.Tables.Reservations}",
            ],
          ]
    Lambda:
      BaseArn:
        !Join [
          ":",
          [
            "arn",
            "aws",
            "lambda",
            !Ref AWS::Region,
            !Ref AWS::AccountId,
            "function",
          ],
        ]
      Functions:
        EvsePush:
          !Join [
            "",
            [
              "${self:custom.environment.${self:provider.stage}}",
              "OdinoEvsePush",
            ],
          ]
        OcppSoap15UnlockConnector:
          !Join [
            "",
            [
              "${self:custom.environment.${self:provider.stage}}",
              "ThorOcppSoap15UnlockConnector",
            ],
          ]
        OcppSoap15RemoteStopTransaction:
          !Join [
            "",
            [
              "${self:custom.environment.${self:provider.stage}}",
              "ThorOcppSoap15RemoteStopTransaction",
            ],
          ]
        OcppSoap15RemoteStartTransaction:
          !Join [
            "",
            [
              "${self:custom.environment.${self:provider.stage}}",
              "ThorOcppSoap15RemoteStartTransaction",
            ],
          ]
        OcppJson16UnlockConnector:
          !Join [
            "",
            [
              "${self:custom.environment.${self:provider.stage}}",
              "ThorOcppJson16UnlockConnector",
            ],
          ]
        OcppJson16RemoteStopTransaction:
          !Join [
            "",
            [
              "${self:custom.environment.${self:provider.stage}}",
              "ThorOcppJson16RemoteStopTransaction",
            ],
          ]
        OcppJson16RemoteStartTransaction:
          !Join [
            "",
            [
              "${self:custom.environment.${self:provider.stage}}",
              "ThorOcppJson16RemoteStartTransaction",
            ],
          ]
        OcppJson16CancelReservation:
          !Join [
            "",
            [
              "${self:custom.environment.${self:provider.stage}}",
              "ThorOcppJson16CancelReservation",
            ],
          ]
      Arns:
        EvsePush:
          !Join [
            ":",
            [
              "${self:custom.environment.Lambda.BaseArn}",
              "${self:custom.environment.Lambda.Functions.EvsePush}",
            ],
          ]
        OcppSoap15UnlockConnector:
          !Join [
            ":",
            [
              "${self:custom.environment.Lambda.BaseArn}",
              "${self:custom.environment.Lambda.Functions.OcppSoap15UnlockConnector}",
            ],
          ]
        OcppSoap15RemoteStopTransaction:
          !Join [
            ":",
            [
              "${self:custom.environment.Lambda.BaseArn}",
              "${self:custom.environment.Lambda.Functions.OcppSoap15RemoteStopTransaction}",
            ],
          ]
        OcppSoap15RemoteStartTransaction:
          !Join [
            ":",
            [
              "${self:custom.environment.Lambda.BaseArn}",
              "${self:custom.environment.Lambda.Functions.OcppSoap15RemoteStartTransaction}",
            ],
          ]
        OcppJson16UnlockConnector:
          !Join [
            ":",
            [
              "${self:custom.environment.Lambda.BaseArn}",
              "${self:custom.environment.Lambda.Functions.OcppJson16UnlockConnector}",
            ],
          ]
        OcppJson16RemoteStopTransaction:
          !Join [
            ":",
            [
              "${self:custom.environment.Lambda.BaseArn}",
              "${self:custom.environment.Lambda.Functions.OcppJson16RemoteStopTransaction}",
            ],
          ]
        OcppJson16RemoteStartTransaction:
          !Join [
            ":",
            [
              "${self:custom.environment.Lambda.BaseArn}",
              "${self:custom.environment.Lambda.Functions.OcppJson16RemoteStartTransaction}",
            ],
          ]
        OcppJson16CancelReservation:
          !Join [
            ":",
            [
              "${self:custom.environment.Lambda.BaseArn}",
              "${self:custom.environment.Lambda.Functions.OcppJson16CancelReservation}",
            ],
          ]
    DomainName:
      Dev: dev.odino.thor.tools
      Prd: odino.thor.tools

  customDomain:
    apiType: rest
    domainName: ${self:custom.environment.DomainName.${self:custom.environment.${self:provider.stage}}}
    basePath: latest
    stage: ${self:provider.stage}
    createRoute53Record: true
    autoDomain: true
    endpointType: edge

functions:
  EvsePush: ${file(./config/functions/CPO/EvsePush.yml)}

resources:
  Resources:
    # IAM Permissions
    IAMEvsePush: ${file(./config/resources/IAM/Lambda/CPO/EvsePush.yml)}
